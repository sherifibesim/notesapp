const mongoose = require("mongoose");
const User = require("../Models/user");
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");


exports.postUser = (req, res, next) => {
  bcrypt.hash(req.body.password, 10).then(hash => {
    const user = new User({
      email: req.body.email,
      password: hash,
    });
    user
      .save()
      .then(data => {
        res.status(201).json({
          message: "User created!",
          data: data
        });
      })
      .catch(err => {
        res.status(500).json({
          error: err
        });
      });
  });
};


exports.login  = (req, res, next) => {
    let fetchedUser;
    User.findOne({ email: req.body.email })
      .then(user => {
        if (!user) {
          return res.status(401).json({
            message: "Auth failed"
          });
        }
        fetchedUser = user;
        return bcrypt.compare(req.body.password,
           user.password);
      })
      .then(data => {
        if (!data) {
          return res.status(401).json({
            message: "Auth failed"
          });
        }
        const token = jwt.sign(
          { email: fetchedUser.email,
             userId: fetchedUser._id
            },
          "secret_this_should_be_longer",
          { expiresIn: "1h" }
        );
        res.status(200).json({
          token: token,
          expiresIn: 3600
        });
      })
      .catch(err => {
        return res.status(401).json({
          message: "Auth failed"
        });
      });
  };
  