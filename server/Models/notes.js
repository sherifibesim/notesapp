const mongoose = require("mongoose");
const notesSchema = mongoose.Schema({
  title: { type: String },
  content: {type: String}
});
module.exports = mongoose.model("Notes", notesSchema);

