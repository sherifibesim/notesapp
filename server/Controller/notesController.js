const mongoose = require("mongoose");
const Notes = require("../Models/notes");

exports.postNotes = (req, res, next) => {
    const { title } = req.body;
    const { content } = req.body;
    const note = new Notes({
      _id: new mongoose.Types.ObjectId(),
      title: title,
      content: content
    });
    note
      .save()
      .then(data=>{
        res.status(201).json({
            message: "Note is created",
            data:data
        })
    })
    .catch(err=>{
      res.status(500).json({
          message: "Error 500!"
      })
      console.log(err)
  })
  };



  exports.getNotes = (req, res, next) => {
    Notes.find()
    .then(data=>{
      if(data.length == 0) {
         return res.status(404).json({
              message: "Role not found"
          });
      }
       res.status(200).json({
          message:"successfuly.",
          data:data
      })
  })
  .catch(err => {
      res.status(500).json({
      message: "Error 500!"
      });
      console.log(err)
    });
  };


  exports.updateNotes = (req, res, next) => {
    const id = req.params.id;
    Notes.findById(id)
    .then( async note => {
      if(!note) {
        return res.status(500).json({
          message: "this was not found"
        });
      }else {
         await Notes.findOneAndUpdate(id, req.body, {new: true});
        res.json({ message: 'Note successfully edited.'});
      }
    });
  }




  exports.deleteNotes = (req, res, next) => {
    Notes.findByIdAndRemove(req.params.id, req.body, function(err) {
     if (err) return next(err);
     res.json('Successfully removed');
    });
   };
  