const mongoose = require("mongoose");
const dbURI =
  "mongodb+srv://besim:8rttemens99@notes.5eoqi.mongodb.net/NotesApp?authSource=admin&replicaSet=atlas-13fc94-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true";

mongoose.set("useCreateIndex", true);
mongoose.connect(dbURI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

mongoose.connection.on("connected", () => {
  console.log(`Mongoose connected to ${dbURI}`);
});
mongoose.connection.on("error", (err) => {
  console.log(`Mongoose connection error: ${err}`);
});
mongoose.connection.on("disconnected", () => {
  console.log("Mongoose disconnected");
});
