const express = require('express');
const router = express.Router();

const notesController = require("../Controller/notesController");
const auth = require("../middleware/check-auth");




router.post("/", notesController.postNotes);
router.get("/", notesController.getNotes);
router.put("/update/:id", notesController.updateNotes);
router.delete("/delete/:id", notesController.deleteNotes)

module.exports = router;