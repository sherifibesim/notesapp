const express = require("express");
const app = express();
// require('dotenv').config();
require("./Models/DB");
const bodyParser = require("body-parser");

const cors = require("cors");
const path = require("path");

// app.use(formidableMiddleware());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// middleware
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(cors());

// Importing routes

const userRoutes = require("./Routes/userRoute");
const notesRoutes = require("./Routes/notesRoute");

// Routes

app.use("/user", userRoutes);
app.use("/notes", notesRoutes);


// throw error when page is not found
app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    },
  });
});
module.exports = app;
